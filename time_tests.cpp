#define CATCH_CONFIG_MAIN

#include <catch.hpp>

#include <lookup.h>

#include <to_string_naive_gmtime_r.h>
#include <format_naive_gmtime_r.h>
#include <format_date.h>
#include <format_date_karma.h>
#include <format_date_lookup.h>
#include <heuristic_format_date_lookup.h>
#include <format_formatter_experiment.h>

#include <parse_timestamp.h>

#include <cassert>
#include <chrono>
#include <chrono_io.h>
#include <random>
#include <string>
#include <thread>
#include <vector>

#define ASSERT(condition, message) \
    do { \
        using namespace date;\
        if (! (condition)) { \
            std::cerr << "Assertion `" #condition "` failed in " << __FILE__ \
                      << " line " << __LINE__ << ": " << message << std::endl; \
            std::terminate(); \
        } \
    } while (false)


constexpr static auto RANGE_FROM  = 0LL;
constexpr static auto RANGE_TO    = 2840140800000000LL;
constexpr static auto SAMPLES     = 1000000;
constexpr static auto THREADS     = 8;

// TODO: Catch macros not thread safe... workaround, replace assert with REQUIRE
void tester()
{
    std::random_device rnd;
    std::mt19937 generator(rnd());
    std::uniform_int_distribution<int64_t> source(RANGE_FROM, RANGE_TO);

    const auto my_format_v2 = [](auto ts){
        using namespace v2;
        using namespace fmt;
        thread_local const auto f = formatter<y_, m_, d_, char, H_, char, M_, char, S_, char, f_, char>(y_(), m_(), d_(), 'T', H_(), ':', M_(), ':', S_(), '.', f_(), 'Z');
        return f.format(ts);
    };

    const auto my_format_v3 = [](auto ts){
        using namespace v3;
        using namespace fmt;
        thread_local const auto f = formatter<y_, m_, d_, char, H_, char, M_, char, S_, char, f_, char>(y_(), m_(), d_(), 'T', H_(), ':', M_(), ':', S_(), '.', f_(), 'Z');
        return f.format(ts);
    };

    for(auto i = 0; i != SAMPLES; ++i)
    {
        const auto ts = std::chrono::microseconds{source(generator)};
        const auto expected = to_string_naive_strftime_snprintf(ts);

        ASSERT((to_string_naive_strftime_snprintf(ts) == expected), ts);
        ASSERT((format_naive_strftime_snprintf(ts) == expected), ts);
        ASSERT((format_date(ts) == expected), ts);
        ASSERT((format_date_pure(ts) == expected), ts);
        ASSERT((format_date_karma1(ts) == expected), ts);
        ASSERT((format_date_karma2(ts) == expected), "");
        ASSERT((format_date_counting_lookup(ts) == expected), ts);
        ASSERT((heuristic_format_date_counting_lookup(ts) == expected), ts);
        ASSERT((format_date_not_counting_lookup(ts) == expected), ts);
        ASSERT((heuristic_format_date_not_counting_lookup(ts) == expected), ts);
        ASSERT((format_date_somewhat_counting_lookup(ts) == expected), ts);
        ASSERT((heuristic_format_date_somewhat_counting_lookup(ts) == expected), ts);

        ASSERT((my_format_v2(ts) == expected), ts);
        ASSERT((my_format_v3(ts) == expected), ts);

        ASSERT((parse_date_ptime(expected.c_str()) == ts), expected);
        ASSERT((parse_date_naive(expected.c_str()) == ts), expected);
        ASSERT((parse_date(expected.c_str()) == ts), expected);
        ASSERT((heuristic_parse_date(expected.c_str()) == ts), expected);
    }
}

TEST_CASE("We are getting consistent results", "[format]")
{
    std::vector<std::thread> threads;
    for(auto i = 0; i != THREADS; ++i)
    {
        threads.emplace_back(tester);
    }
    for(auto & t : threads)
    {
        t.join();
    }
}

TEST_CASE("Helpers methods", "helpers")
{
    REQUIRE(atou<1>("1") == 1);
    REQUIRE(atou<2>("12") == 12);
    REQUIRE(atou<3>("123") == 123);
    REQUIRE(atou<4>("1234") == 1234);
    REQUIRE(atou<5>("12345") == 12345);
    REQUIRE(atou<6>("123456") == 123456);
}
