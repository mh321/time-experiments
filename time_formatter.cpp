#define CATCH_CONFIG_MAIN

#include <catch.hpp>

#include <format_formatter_experiment.h>


TEST_CASE("Formatter v2", "[format]")
{
    using namespace v2;
    using namespace std::chrono_literals;
    using namespace v2::fmt;
    const auto my_format = formatter<y_, m_, d_, char, H_, char, M_, char, S_, char, f_, char>(y_(), m_(), d_(), 'T', H_(), ':', M_(), ':', S_(), '.', f_(), 'Z');
    {
        const auto expected = std::string{"20161012T20:47:39.123Z"};
        const auto actual = my_format.format(1476305259123ms);
        REQUIRE( expected == actual );
    }
    {
        const auto expected = std::string{"20161012T20:47:39.123456Z"};
        const auto actual = my_format.format(1476305259123456us);
        REQUIRE( expected == actual );
    }
    {
        const auto expected = std::string{"20161012T20:47:39.123456789Z"};
        const auto actual = my_format.format(1476305259123456789ns);
        REQUIRE( expected == actual );
    }
}

TEST_CASE("Formatter v3", "[format]")
{
    using namespace v3;
    using namespace std::chrono_literals;
    using namespace v3::fmt;
    const auto my_format = formatter<y_, m_, d_, char, H_, char, M_, char, S_, char, f_, char>(y_(), m_(), d_(), 'T', H_(), ':', M_(), ':', S_(), '.', f_(), 'Z');
    {
        const auto expected = std::string{"20161012T20:47:39.123Z"};
        const auto actual = my_format.format(1476305259123ms);
        REQUIRE( expected == actual );
    }
    {
        const auto expected = std::string{"20161012T20:47:39.123456Z"};
        const auto actual = my_format.format(1476305259123456us);
        REQUIRE( expected == actual );
    }
    {
        const auto expected = std::string{"20161012T20:47:39.123456789Z"};
        const auto actual = my_format.format(1476305259123456789ns);
        REQUIRE( expected == actual );
    }
}

