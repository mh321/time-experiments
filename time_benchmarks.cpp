#include <benchmark/benchmark.h>

#include <to_string_naive_gmtime_r.h>
#include <format_naive_gmtime_r.h>
#include <format_date.h>
#include <format_date_karma.h>
#include <format_date_lookup.h>
#include <heuristic_format_date_lookup.h>
#include <format_formatter_experiment.h>

#include <parse_timestamp.h>

#include <boost/config.hpp>

#include <algorithm>
#include <array>
#include <chrono>
#include <cstring>
#include <memory>
#include <random>
#include <string>
#include <vector>

using namespace std::chrono_literals;

// Starting time for test data with predictable and random-walk inputs
constexpr static auto START_TIME = 1474913552000000LL;

// Time range for randomly generated timestamps
constexpr static auto RANGE_FROM = 0LL;
constexpr static auto RANGE_TO   = 2840140800000000LL;

// Total number of samples to use
constexpr static auto SAMPLES = 1 << 20;

// Number of threads to use in threaded benchmark
constexpr static auto THREAD_COUNT = 4;

// test_data laid out in memory as struct of arrays
struct test_data
{
    char * datetime(size_t idx) const { return datetimes.get() + idx * (TIMESTAMP_LENGTH + 1); }

    std::chrono::microseconds timestamps[SAMPLES];
    std::unique_ptr<char[]> datetimes = std::make_unique<char[]>(SAMPLES * (TIMESTAMP_LENGTH + 1));
};

// Predictable input: start at some fixed point in time and every sample increase by dT microseconds
const static test_data PREDICTABLE_INPUT = [](){
    test_data result;
    auto start = std::chrono::microseconds{START_TIME};
    for(auto i = 0; i != SAMPLES; ++i)
    {
        result.timestamps[i] = start;
        const char * datetime = format_date_not_counting_lookup(start);
        std::memcpy(result.datetime(i), datetime, TIMESTAMP_LENGTH);
        result.datetime(i)[TIMESTAMP_LENGTH] = '\0';
        start += 11us;
    }
    return result;
}();

// Random input - sample uniformly from a predefined range
const static test_data RANDOM_INPUT = [](){
    std::random_device rnd;
    std::mt19937 generator(rnd());
    std::uniform_int_distribution<int64_t> source(RANGE_FROM, RANGE_TO);
    test_data result;
    for(auto i = 0; i != SAMPLES; ++i)
    {
        result.timestamps[i] = std::chrono::microseconds{source(generator)};
        const char * datetime = format_date_not_counting_lookup(result.timestamps[i]);
        std::memcpy(result.datetime(i), datetime, TIMESTAMP_LENGTH);
        result.datetime(i)[TIMESTAMP_LENGTH] = '\0';
    }
    return result;
}();

// Perform a random walk starting from a given point in time.
const static test_data RANDOM_WALK_INPUT = [](){
    std::random_device rnd;
    std::mt19937 generator(rnd());
    std::normal_distribution<> source;
    test_data result;
    for(auto i = 0; i != SAMPLES; ++i)
    {
        result.timestamps[i] = std::chrono::microseconds{static_cast<int64_t>(source(generator) * 1000000 * 3600 * 2)};
    }
    result.timestamps[0] = std::chrono::microseconds{START_TIME};
    std::partial_sum(std::begin(result.timestamps), std::end(result.timestamps), std::begin(result.timestamps));

    for(auto i = 0; i != SAMPLES; ++i)
    {
        const char * datetime = format_date_not_counting_lookup(result.timestamps[i]);
        std::memcpy(result.datetime(i), datetime, TIMESTAMP_LENGTH);
        result.datetime(i)[TIMESTAMP_LENGTH] = '\0';
    }
    return result;
}();

/**
 * \brief main benchmark function used to measure std::chrono::microseconds -> datetime string conversion
 * \tparam FunctionUnderTest Function that is being measured
 * \tparam ResultConsumer Validation function that actually does something with the result (e.g. compare against expected value)
 */
template<typename FunctionUnderTest, typename ResultConsumer>
void BM_format(benchmark::State & state, const test_data & data, FunctionUnderTest tes_fun, ResultConsumer consumer)
{
    size_t i = 0;
    while(state.KeepRunning())
    {
        const auto ts = data.timestamps[i];
        auto res = tes_fun(ts);
        if(BOOST_UNLIKELY(!consumer(res, data.datetime(i))))
        {
            std::abort();
        }
        i = (i + 1) & (SAMPLES - 1);
    }
}

// Preprocessor incantations to generate benchmark functions.
// If that were to get any more complicated BOOST_PP would be a good option

#define CONCAT_IMPL__( x, y ) x##y
#define MACRO_CONCAT__( x, y ) CONCAT_IMPL__( x, y )

#define MAKE_BENCHMARK_FUN_IMPL__(bm_fun, unique_lambda_name, fun, input, verify)\
    auto unique_lambda_name = [](auto i){return fun(i); };\
    BENCHMARK_CAPTURE(bm_fun, fun##_##input, input, unique_lambda_name, verify)->Threads(1)->Threads(4)->UseRealTime();

#define MAKE_BENCHMARK_FUN(bm_fun, fun, input, verify)\
    MAKE_BENCHMARK_FUN_IMPL__(bm_fun, MACRO_CONCAT__(fun , __COUNTER__), fun, input, verify)

// Benchmark a function fun using three different data sets and a given verify function
#define MAKE_BENCHMARK(bm_fun, fun, verify)\
    MAKE_BENCHMARK_FUN(bm_fun, fun, PREDICTABLE_INPUT, verify)\
    MAKE_BENCHMARK_FUN(bm_fun, fun, RANDOM_WALK_INPUT, verify)\
    MAKE_BENCHMARK_FUN(bm_fun, fun, RANDOM_INPUT, verify)

auto string_verify = [](const std::string & result, const char * expected) {
    return std::equal(result.begin(), result.end(), expected);
};

auto cstring_verify = [](const char * result, const char * expected) {
    return std::memcmp(result, expected, TIMESTAMP_LENGTH) == 0;
};

MAKE_BENCHMARK(BM_format, to_string_naive_strftime_snprintf, string_verify)
MAKE_BENCHMARK(BM_format, format_naive_strftime_snprintf, cstring_verify)
MAKE_BENCHMARK(BM_format, format_date, cstring_verify)
MAKE_BENCHMARK(BM_format, format_date_pure, string_verify)
MAKE_BENCHMARK(BM_format, format_date_karma1, cstring_verify)
MAKE_BENCHMARK(BM_format, format_date_karma2, cstring_verify)
MAKE_BENCHMARK(BM_format, format_date_counting_lookup, cstring_verify)
MAKE_BENCHMARK(BM_format, heuristic_format_date_counting_lookup, cstring_verify)
MAKE_BENCHMARK(BM_format, format_date_not_counting_lookup, cstring_verify)
MAKE_BENCHMARK(BM_format, heuristic_format_date_not_counting_lookup, cstring_verify)
MAKE_BENCHMARK(BM_format, format_date_somewhat_counting_lookup, cstring_verify)
MAKE_BENCHMARK(BM_format, heuristic_format_date_somewhat_counting_lookup, cstring_verify)

const auto my_format_v2 = [](auto ts){
    using namespace v2;
    using namespace fmt;
    thread_local const auto f = formatter<y_, m_, d_, char, H_, char, M_, char, S_, char, f_, char>(y_(), m_(), d_(), 'T', H_(), ':', M_(), ':', S_(), '.', f_(), 'Z');
    return f.format(ts);
};

const auto my_format_v3 = [](auto ts){
    using namespace v3;
    using namespace fmt;
    thread_local const auto f = formatter<y_, m_, d_, char, H_, char, M_, char, S_, char, f_, char>(y_(), m_(), d_(), 'T', H_(), ':', M_(), ':', S_(), '.', f_(), 'Z');
    return f.format(ts);
};


MAKE_BENCHMARK(BM_format, my_format_v2, cstring_verify)
MAKE_BENCHMARK(BM_format, my_format_v3, cstring_verify)

/**
 * \brief main benchmark function used to measure datetime string -> std::chrono::microseconds conversion
 * \tparam FunctionUnderTest Function that is being measured
 * \tparam ResultConsumer Validation function that actually does something with the result (e.g. compare against expected value)
 */
template<typename FunctionUnderTest, typename ResultConsumer>
void BM_parse(benchmark::State & state, const test_data & data, FunctionUnderTest tes_fun, ResultConsumer consumer)
{
    size_t i = 0;
    while(state.KeepRunning())
    {
        const auto str = data.datetime(i);
        auto res = tes_fun(str);
        if(BOOST_UNLIKELY(!consumer(res, data.timestamps[i])))
        {
            std::abort();
        }
        i = (i + 1) & (SAMPLES - 1);
    }
}

auto microseconds_verify = [](std::chrono::microseconds result, std::chrono::microseconds expected) {
    return result == expected;
};

MAKE_BENCHMARK(BM_parse, parse_date_ptime, microseconds_verify)
MAKE_BENCHMARK(BM_parse, parse_date_naive, microseconds_verify)
MAKE_BENCHMARK(BM_parse, parse_date, microseconds_verify)
MAKE_BENCHMARK(BM_parse, heuristic_parse_date, microseconds_verify)

BENCHMARK_MAIN()
