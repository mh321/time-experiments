#pragma once

#include <boost/config.hpp>
#include <cstdint>

constexpr static char HUNDRED_NUMBERS[200] = {
    '0','0','1','0','2','0','3','0','4','0','5','0','6','0','7','0','8','0','9','0',
    '0','1','1','1','2','1','3','1','4','1','5','1','6','1','7','1','8','1','9','1',
    '0','2','1','2','2','2','3','2','4','2','5','2','6','2','7','2','8','2','9','2',
    '0','3','1','3','2','3','3','3','4','3','5','3','6','3','7','3','8','3','9','3',
    '0','4','1','4','2','4','3','4','4','4','5','4','6','4','7','4','8','4','9','4',
    '0','5','1','5','2','5','3','5','4','5','5','5','6','5','7','5','8','5','9','5',
    '0','6','1','6','2','6','3','6','4','6','5','6','6','6','7','6','8','6','9','6',
    '0','7','1','7','2','7','3','7','4','7','5','7','6','7','7','7','8','7','9','7',
    '0','8','1','8','2','8','3','8','4','8','5','8','6','8','7','8','8','8','9','8',
    '0','9','1','9','2','9','3','9','4','9','5','9','6','9','7','9','8','9','9','9',
};

constexpr static int LENGTH_LOOKUP[] = {
    0,
    10,
    100,
    1000,
    10000,
    100000,
    1000000,
    10000000,
    100000000,
    1000000000
};

///////////////////////////////////////////////////////////////////////

inline int count_digits(int32_t n)
{
    // Precondition: n is a positive value
    int t = (32 - __builtin_clz(n | 1)) * 1233 >> 12;
    return t - (n < LENGTH_LOOKUP[t]) + 1;
}

inline int count_digits_unroll(int32_t n)
{
    // Precondition int32_t is positive
    int_fast32_t result = 1;
    for (;;)
    {
        if (n < 10)    return result;
        if (n < 100)   return result + 1;
        if (n < 1000)  return result + 2;
        if (n < 10000) return result + 3;
        n /= 10000;
        result += 4;
    }
}

inline bool utoa_lookup(char * buffer, int limit, int32_t n)
{
    // Precondition: n is a positive value
    const auto length = count_digits(n);
    if(BOOST_UNLIKELY(length > limit))
        return false;

    for(auto i = 0; i < limit - length; ++i)
        *(buffer+i) = '0';

    buffer += limit;

    while(n >= 100)
    {
        const auto i = (n % 100) << 1;
        n /= 100;
        *--buffer = HUNDRED_NUMBERS[i];
        *--buffer = HUNDRED_NUMBERS[i + 1];
    }
    if(n < 10)
    {
        *--buffer = char(n) + '0';
    }
    else
    {
        const auto i = n << 1;
        *--buffer = HUNDRED_NUMBERS[i];
        *--buffer = HUNDRED_NUMBERS[i + 1];
    }
    return true;
}


///////////////////////////////////////////////////////////////////////

inline bool utoa_lookup_faster2(char * buffer, int32_t n)
{
    // Precondition: n is a positive value
    buffer += 2;
    const auto i = n << 1;
    *--buffer = HUNDRED_NUMBERS[i];
    *--buffer = HUNDRED_NUMBERS[i + 1];
    return !(n / 100);
}

inline bool utoa_lookup_faster4(char * buffer, int32_t n)
{
    // Precondition: n is a positive value
    buffer += 4;
    {
        const auto i = (n % 100) << 1;
        n /= 100;
        *--buffer = HUNDRED_NUMBERS[i];
        *--buffer = HUNDRED_NUMBERS[i + 1];
    }
    {
        const auto i = n << 1;
        *--buffer = HUNDRED_NUMBERS[i];
        *--buffer = HUNDRED_NUMBERS[i + 1];
    }
    return !(n / 100);
}
inline bool utoa_lookup_faster6(char * buffer, int32_t n)
{
    // Precondition: n is a positive value
    buffer += 6;
    {
        const auto i = (n % 100) << 1;
        n /= 100;
        *--buffer = HUNDRED_NUMBERS[i];
        *--buffer = HUNDRED_NUMBERS[i + 1];
    }
    {
        const auto i = (n % 100) << 1;
        n /= 100;
        *--buffer = HUNDRED_NUMBERS[i];
        *--buffer = HUNDRED_NUMBERS[i + 1];
    }
    {
        const auto i = n << 1;
        *--buffer = HUNDRED_NUMBERS[i];
        *--buffer = HUNDRED_NUMBERS[i + 1];
    }
    return !(n / 100);
}

///////////////////////////////////////////////////////////////////////

inline bool utoa_lookup_faster_even_limit(char * buffer, int limit, int32_t n)
{
    // Precondition: n is a positive value
    buffer += limit;
    for(auto count = 0; count != limit; count+=2)
    {
        const auto i = (n % 100) << 1;
        n /= 100;
        *--buffer = HUNDRED_NUMBERS[i];
        *--buffer = HUNDRED_NUMBERS[i + 1];
    }
    return !n;
}

///////////////////////////////////////////////////////////////////////

constexpr static int32_t POW10_LOOKUP[] = {
    1,
    10,
    100,
    1000,
    10000,
    100000,
    1000000,
    10000000,
    100000000,
    1000000000
};

template<int N>
__attribute__((optimize("unroll-loops")))
inline int atou(const char * str)
{
    // Precondition: str has a positive string that fits in an int
    unsigned result = 0;
    for(auto i = 0; i != N; ++i)
    {
        result += (str[i] - '0') * POW10_LOOKUP[N-i-1];
    }
    return result;
}

///////////////////////////////////////////////////////////////////////
