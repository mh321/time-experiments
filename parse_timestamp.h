#pragma once

#include "constants.h"

#include <chrono>

std::chrono::microseconds BENCHMARK_NOINLINE parse_date_ptime(const char * str);

std::chrono::microseconds BENCHMARK_NOINLINE parse_date_naive(const char * str);

std::chrono::microseconds BENCHMARK_NOINLINE parse_date(const char * str);

std::chrono::microseconds BENCHMARK_NOINLINE heuristic_parse_date(const char * str);
