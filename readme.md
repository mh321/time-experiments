## Context

* [Get that date](https://mh321.bitbucket.io/post/cxx/get_that_date_p4/)
* [Parse that date](https://mh321.bitbucket.io/post/cxx/parse_that_date/)

## Building:

```bash
git clone git@bitbucket.org:mh321/time-experiments.git
mkdir build-time-experiments
cd build-time-experiments
cmake -G "Unix Makefiles" -DVARIANT=release ../time-experiments/
make -j24
```

## Running

### Tests

```bash
./time_tests
```

### Benchmarks

```bash
./time_benchmarks
```
