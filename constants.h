#pragma once

#if defined(__GNUC__)
# define BENCHMARK_NOINLINE __attribute__((noinline))
#else
# define BENCHMARK_NOINLINE
#endif

// input:  1474913552123456
//
// output:
//
// 0         1         2
// 0123456789012345678901234   <- length
// 20160926T18:12:32.123456Z   <- timestamp


constexpr static auto TIME_OFFSET = 9;
constexpr static auto DATE_TIME_LENGTH = 17;
constexpr static auto TIMESTAMP_LENGTH = 25;

constexpr static auto YEAR_OFFSET = 0;
constexpr static auto MONTH_OFFSET = 4;
constexpr static auto DAY_OFFSET = 6;
constexpr static auto HOURS_OFFSET = 9;
constexpr static auto MINUTES_OFFSET = 12;
constexpr static auto SECONDS_OFFSET = 15;
constexpr static auto USEC_OFFSET = 18;
