#pragma once

#include "constants.h"

#include <chrono>

const char * BENCHMARK_NOINLINE format_date_karma1(std::chrono::microseconds micros);

const char * BENCHMARK_NOINLINE format_date_karma2(std::chrono::microseconds micros);
