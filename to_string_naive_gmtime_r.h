#pragma once

#include "constants.h"

#include <chrono>
#include <string>

std::string BENCHMARK_NOINLINE to_string_naive_strftime_snprintf(std::chrono::microseconds micros);
