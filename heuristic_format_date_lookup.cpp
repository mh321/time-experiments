#include "heuristic_format_date_lookup.h"
#include "lookup.h"

#include <boost/config.hpp>

#include <date.h>
#include <chrono>

const char * BENCHMARK_NOINLINE heuristic_format_date_counting_lookup(std::chrono::microseconds micros)
{
    thread_local char buffer[32] = "20160926T18:12:32.123456Z\0";

    using namespace std::chrono;
    using namespace date;
    thread_local sys_days previous_dp;

    time_point<system_clock, microseconds> tp{micros};

    bool ok = true;
    const auto dp = floor<days>(tp);
    if(dp != previous_dp)
    {
        previous_dp = dp;
        const auto ymd = year_month_day(dp);
        ok &= utoa_lookup(buffer + YEAR_OFFSET, 4, int(ymd.year()));
        ok &= utoa_lookup(buffer + MONTH_OFFSET, 2, unsigned(ymd.month()));
        ok &= utoa_lookup(buffer + DAY_OFFSET, 2, unsigned(ymd.day()));
    }

    const auto tod = make_time(tp - previous_dp);
    const auto usec = micros % 1000000;
    ok &= utoa_lookup(buffer + HOURS_OFFSET, 2, unsigned(tod.hours().count()));
    ok &= utoa_lookup(buffer + MINUTES_OFFSET, 2, unsigned(tod.minutes().count()));
    ok &= utoa_lookup(buffer + SECONDS_OFFSET, 2, unsigned(tod.seconds().count()));
    ok &= utoa_lookup(buffer + USEC_OFFSET, 6, unsigned(usec.count()));

    if(BOOST_UNLIKELY(!ok)) buffer[0] = '\0';
    return buffer;
}

const char * BENCHMARK_NOINLINE heuristic_format_date_not_counting_lookup(std::chrono::microseconds micros)
{
    thread_local char buffer[32] = "20160926T18:12:32.123456Z\0";

    using namespace std::chrono;
    using namespace date;
    thread_local sys_days previous_dp;

    time_point<system_clock, microseconds> tp{micros};

    bool ok = true;
    const auto dp = floor<days>(tp);
    if(dp != previous_dp)
    {
        previous_dp = dp;
        const auto ymd = year_month_day(dp);
        ok &= utoa_lookup_faster4(buffer + YEAR_OFFSET, int(ymd.year()));
        ok &= utoa_lookup_faster2(buffer + MONTH_OFFSET, unsigned(ymd.month()));
        ok &= utoa_lookup_faster2(buffer + DAY_OFFSET, unsigned(ymd.day()));
    }

    const auto tod = make_time(tp - previous_dp);
    const auto usec = micros % 1000000;
    ok &= utoa_lookup_faster2(buffer + HOURS_OFFSET, unsigned(tod.hours().count()));
    ok &= utoa_lookup_faster2(buffer + MINUTES_OFFSET, unsigned(tod.minutes().count()));
    ok &= utoa_lookup_faster2(buffer + SECONDS_OFFSET, unsigned(tod.seconds().count()));
    ok &= utoa_lookup_faster6(buffer + USEC_OFFSET, unsigned(usec.count()));

    if(BOOST_UNLIKELY(!ok)) buffer[0] = '\0';
    return buffer;
}


const char * BENCHMARK_NOINLINE heuristic_format_date_somewhat_counting_lookup(std::chrono::microseconds micros)
{
    thread_local char buffer[32] = "20160926T18:12:32.123456Z\0";

    using namespace std::chrono;
    using namespace date;
    thread_local sys_days previous_dp;

    time_point<system_clock, microseconds> tp{micros};

    bool ok = true;
    const auto dp = floor<days>(tp);
    if(dp != previous_dp)
    {
        previous_dp = dp;
        const auto ymd = year_month_day(dp);
        ok &= utoa_lookup_faster_even_limit(buffer + YEAR_OFFSET, 4, int(ymd.year()));
        ok &= utoa_lookup_faster_even_limit(buffer + MONTH_OFFSET, 2, unsigned(ymd.month()));
        ok &= utoa_lookup_faster_even_limit(buffer + DAY_OFFSET, 2, unsigned(ymd.day()));
    }

    const auto tod = make_time(tp - previous_dp);
    const auto usec = micros % 1000000;
    ok &= utoa_lookup_faster_even_limit(buffer + HOURS_OFFSET, 2, unsigned(tod.hours().count()));
    ok &= utoa_lookup_faster_even_limit(buffer + MINUTES_OFFSET, 2, unsigned(tod.minutes().count()));
    ok &= utoa_lookup_faster_even_limit(buffer + SECONDS_OFFSET, 2, unsigned(tod.seconds().count()));
    ok &= utoa_lookup_faster_even_limit(buffer + USEC_OFFSET, 6, unsigned(usec.count()));

    if(BOOST_UNLIKELY(!ok)) buffer[0] = '\0';
    return buffer;
}
