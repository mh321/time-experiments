#pragma once

#include "constants.h"

#include <chrono>

const char * BENCHMARK_NOINLINE format_naive_strftime_snprintf(std::chrono::microseconds micros);
