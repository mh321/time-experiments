#include "format_naive_gmtime_r.h"

#include <boost/config.hpp>

#include <ctime>
#include <cstdio>
#include <cstdlib>

const char * BENCHMARK_NOINLINE format_naive_strftime_snprintf(std::chrono::microseconds micros)
{
    thread_local std::tm tm;
    thread_local char buffer[32];

    const auto sec = static_cast<time_t>(micros.count() / 1000000);
    const auto usec = micros.count() % 1000000;


    bool ok = gmtime_r(&sec, &tm);
    ok &= std::strftime(buffer, sizeof(buffer), "%Y%m%dT%H:%M:%S", &tm);
    ok &= std::snprintf(buffer + DATE_TIME_LENGTH, sizeof(buffer) - DATE_TIME_LENGTH, ".%06ldZ", usec) > 0;
    if(BOOST_UNLIKELY(!ok)) buffer[0] = '\0';
    return buffer;
}
