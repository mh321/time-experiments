#include "parse_timestamp.h"
#include "lookup.h"

#include <boost/date_time.hpp>

#include <date.h>
#include <cstring>

#include <sstream>

std::chrono::microseconds BENCHMARK_NOINLINE parse_date_ptime(const char * str)
{
    namespace bpt = boost::posix_time;
    static const bpt::ptime time_t_epoch{boost::gregorian::date{1970,1,1}};
    thread_local std::stringstream ss = [](){
        using boost::posix_time::time_input_facet;
        using std::locale;

        std::stringstream result;
        result.imbue(locale{locale::classic(), new time_input_facet{"%Y%m%dT%H:%M:%S%F"}});
        return result;
    }();

    ss.clear();
    ss.str(str);
    bpt::ptime time;
    ss >> time;

    const auto diff = time - time_t_epoch;
#ifdef BOOST_DATE_TIME_POSIX_TIME_STD_CONFIG
    return std::chrono::nanoseconds{diff.ticks()};
#else
    return std::chrono::microseconds{diff.ticks()};
#endif
}

std::chrono::microseconds BENCHMARK_NOINLINE parse_date_naive(const char * str)
{
    int y, m, d, h, min, sec, usec;
    if(7 != sscanf(str, "%4d%2d%2dT%d:%d:%d.%d", &y, &m, &d, &h, &min, &sec, &usec))
    {
        return std::chrono::microseconds{0};
    }
    using namespace std::chrono;
    using namespace date;
    const auto ymd = year(y)/m/d;
    time_point<system_clock, microseconds> tp = sys_days(ymd) +
            hours{h} + minutes{min} + seconds{sec} + microseconds(usec);
    return tp.time_since_epoch();
}

std::chrono::microseconds BENCHMARK_NOINLINE parse_date(const char * str)
{
    // Preconditions: str points to beginning of correctly formatted string (at least sufficient length)

    using namespace std::chrono;
    using namespace date;
    const auto y = atou<4>(str + YEAR_OFFSET);
    const auto m = atou<2>(str + MONTH_OFFSET);
    const auto d = atou<2>(str + DAY_OFFSET);
    const auto h = atou<2>(str + HOURS_OFFSET);
    const auto min = atou<2>(str + MINUTES_OFFSET);
    const auto sec = atou<2>(str + SECONDS_OFFSET);
    const auto usec = atou<6>(str + USEC_OFFSET);

    const auto ymd = year(y)/m/d;
    time_point<system_clock, microseconds> tp = sys_days(ymd) +
            hours{h} + minutes{min} + seconds{sec} + microseconds(usec);
    return tp.time_since_epoch();
}

std::chrono::microseconds BENCHMARK_NOINLINE heuristic_parse_date(const char * str)
{
    // Preconditions: str points to beginning of correctly formatted string (at least sufficient length)
    using namespace std::chrono;
    using namespace date;
    constexpr static auto YMD_LENGTH = 8;
    thread_local char last_ymd[YMD_LENGTH];
    thread_local time_point<system_clock, microseconds> last_ymd_tp;

    if(BOOST_UNLIKELY(std::memcmp(last_ymd, str, YMD_LENGTH)) != 0)
    {
        const auto y = atou<4>(str + YEAR_OFFSET);
        const auto m = atou<2>(str + MONTH_OFFSET);
        const auto d = atou<2>(str + DAY_OFFSET);
        last_ymd_tp = sys_days(year(y)/m/d);
        std::memcpy(last_ymd, str, YMD_LENGTH);
    }
    const auto h = atou<2>(str + HOURS_OFFSET);
    const auto min = atou<2>(str + MINUTES_OFFSET);
    const auto sec = atou<2>(str + SECONDS_OFFSET);
    const auto usec = atou<6>(str + USEC_OFFSET);
    time_point<system_clock, microseconds> tp = last_ymd_tp + hours{h} + minutes{min} + seconds{sec} + microseconds(usec);

    return tp.time_since_epoch();
}
