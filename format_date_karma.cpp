#include "format_date_karma.h"

#include <boost/config.hpp>
#include <boost/spirit/include/karma.hpp>

#include <date.h>

const char * BENCHMARK_NOINLINE format_date_karma1(std::chrono::microseconds micros)
{
    thread_local char buffer[32];

    using namespace std::chrono;
    using namespace date;
    time_point<system_clock, microseconds> tp{micros};

    const auto dp = floor<days>(tp);
    const auto ymd = year_month_day(dp);
    const auto tod = make_time(tp - dp);
    const auto usec = micros.count() % 1000000;

    using namespace boost::spirit;
    char * it = buffer;
    bool ok = karma::generate(it,
                    karma::right_align(4, '0')[karma::int_] <<
                    karma::right_align(2, '0')[karma::uint_] <<
                    karma::right_align(2, '0')[karma::uint_] << 'T' <<
                    karma::right_align(2, '0')[karma::int_] << ':' <<
                    karma::right_align(2, '0')[karma::int_] << ':' <<
                    karma::right_align(2, '0')[karma::int_] << '.' <<
                    karma::right_align(6, '0')[karma::int_] << 'Z',
            int(ymd.year()), unsigned(ymd.month()), unsigned(ymd.day()),
            tod.hours().count(), tod.minutes().count(), tod.seconds().count(), usec);

    if(BOOST_UNLIKELY(!ok)) buffer[0] = '\0';
    return buffer;
}

const char * BENCHMARK_NOINLINE format_date_karma2(std::chrono::microseconds micros)
{
    thread_local char buffer[32] = "20160926T18:12:32.123456Z\0";

    using namespace std::chrono;
    using namespace date;
    time_point<system_clock, microseconds> tp{micros};

    const auto dp = floor<days>(tp);
    const auto ymd = year_month_day(dp);
    const auto tod = make_time(tp - dp);
    const auto usec = micros.count() % 1000000;

    using namespace boost::spirit;
    char * it = buffer;
    bool ok = karma::generate(it, karma::right_align(4, '0')[karma::int_], int(ymd.year()));
    ok &= karma::generate(it, karma::right_align(2, '0')[karma::uint_], unsigned(ymd.month()));
    ok &= karma::generate(it, karma::right_align(2, '0')[karma::uint_], unsigned(ymd.day()));
    ++it;
    ok &= karma::generate(it, karma::right_align(2, '0')[karma::int_], tod.hours().count());
    ++it;
    ok &= karma::generate(it, karma::right_align(2, '0')[karma::int_], tod.minutes().count());
    ++it;
    ok &= karma::generate(it, karma::right_align(2, '0')[karma::int_], tod.seconds().count());
    ++it;
    ok &= karma::generate(it, karma::right_align(6, '0')[karma::int_], usec);

    if(BOOST_UNLIKELY(!ok)) buffer[0] = '\0';
    return buffer;
}
