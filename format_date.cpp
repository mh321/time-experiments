#include "format_date.h"

#include <boost/config.hpp>

#include <date.h>

const char * BENCHMARK_NOINLINE format_date(std::chrono::microseconds micros)
{
    thread_local char buffer[32];
    thread_local std::tm tm;

    using namespace std::chrono;
    using namespace date;
    time_point<system_clock, microseconds> tp{micros};

    const auto dp = floor<days>(tp);
    const auto ymd = year_month_day(dp);
    const auto tod = make_time(tp - dp);
    const auto usec = micros % 1000000;

    tm.tm_sec   = tod.seconds().count();
    tm.tm_min   = tod.minutes().count();
    tm.tm_hour  = tod.hours().count();
    tm.tm_mday  = unsigned(ymd.day());
    tm.tm_mon   = unsigned(ymd.month()) - 1u;
    tm.tm_year  = int(ymd.year()) - 1900;

    bool ok = std::strftime(buffer, sizeof(buffer), "%Y%m%dT%H:%M:%S", &tm);
    ok &= std::snprintf(buffer + DATE_TIME_LENGTH, sizeof(buffer) - DATE_TIME_LENGTH, ".%06ldZ", usec.count()) > 0;

    if(BOOST_UNLIKELY(!ok)) buffer[0] = '\0';
    return buffer;
}

std::string BENCHMARK_NOINLINE format_date_pure(std::chrono::microseconds micros)
{
    using namespace std::chrono;
    using namespace date;
    time_point<system_clock, microseconds> tp{micros};
    return format("%Y%m%dT%TZ", tp);
}
