#include "to_string_naive_gmtime_r.h"

#include <boost/config.hpp>

#include <ctime>
#include <cstdio>
#include <cstdlib>

std::string BENCHMARK_NOINLINE to_string_naive_strftime_snprintf(std::chrono::microseconds micros)
{
    const auto sec = static_cast<time_t>(micros.count() / 1000000);
    const auto usec = micros.count() % 1000000;

    std::tm tm;
    bool ok = gmtime_r(&sec, &tm);


    char buffer[32];
    ok &= std::strftime(buffer, sizeof(buffer), "%Y%m%dT%H:%M:%S", &tm);
    ok &= std::snprintf(buffer + DATE_TIME_LENGTH, sizeof(buffer) - DATE_TIME_LENGTH, ".%06ldZ", usec) > 0;
    return std::string(buffer, BOOST_LIKELY(ok) ? TIMESTAMP_LENGTH : 0);
}
