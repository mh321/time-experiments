#pragma once

#include "constants.h"

#include <chrono>

const char * BENCHMARK_NOINLINE format_date_counting_lookup(std::chrono::microseconds micros);

const char * BENCHMARK_NOINLINE format_date_not_counting_lookup(std::chrono::microseconds micros);

const char * BENCHMARK_NOINLINE format_date_somewhat_counting_lookup(std::chrono::microseconds micros);
