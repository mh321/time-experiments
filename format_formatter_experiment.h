#pragma once

#include "lookup.h"

#include "date.h"

#include <limits>
#include <chrono>
#include <tuple>
#include <type_traits>
#include <utility>

namespace detail
{
    template<typename T, typename F, std::size_t... Is>
    void for_each(T&& t, F&& f, std::index_sequence<Is...>)
    {
        auto l = { (f(std::get<Is>(t)), 0)... };
        (void)l;
    }
}

template<typename... Ts, typename F>
void for_each_in_tuple(std::tuple<Ts...> const& t, F&& f)
{
    detail::for_each(t, f, std::make_index_sequence<sizeof...(Ts)>());
}

namespace detail {
    template<class F, class... Fs>
    struct overload : F, overload<Fs...>
    {
        using F::operator();
        using overload<Fs...>::operator();

        overload(F&& f, Fs&&... fs)
          : F(std::move(f))
          , overload<Fs...>(std::move(fs)...)
        {}
    };

    template<class F>
    struct overload<F> : F
    {
        using F::operator();

        overload(F&& f)
          : F(std::move(f))
        {}
    };
}

namespace v2
{
namespace fmt
{
    struct y_ {};
    struct m_ {};
    struct d_ {};
    struct H_ {};
    struct M_ {};
    struct S_ {};
    struct f_ {};
}


auto const make_overload = [](auto... fs)
{
    return detail::overload<decltype(fs)...>{std::move(fs)...};
};


template<typename... T>
struct formatter
{
    constexpr formatter(T&&... args)
        : formatters{std::forward<T>(args)...}
        , buffer{}
    {
    }

    template<typename Duration>
    const char * format(Duration duration) const
    {
        using namespace std::chrono;
        using namespace date;

        const time_point<system_clock, Duration> tp{duration};
        const auto dp = floor<days>(tp);
        const auto ymd = year_month_day(dp);
        const auto tod = make_time(tp - dp);

        // TODO: GCC not happy with static:
        // https://godbolt.org/#z:OYLghAFBqd5QCxAYwPYBMCmBRdBLAF1QCcAaPECAKxAEZTUAHAvVAOwGdK0AbVAV2J4OmAIIcAtiADkABmmk0Exnh6YA8mwDCCAIZtgmGfICUpDgOLIj0gKQAmAMx42yHvywBqW460F%2BjGo%2B2Layog7Oru5ePlr8LDyEAJ7BoeFhbLoSmByMutaeWAS6qmm2AOwAQmmetZ4EmMo8ug2xBEmMmJnZngAqpPUdXVmYngBiAxwE6CAgHHgAXpgA%2BgQAdBueAJIcqWF1ngBuqHjongBmJMuY%2BQgQvQ4AbE/1A2NPL%2BeT07MuWAAeyxEAEd%2BF1rLEdhs1sETDU6hVqvsDnVdPFUJ4eN5HAARbxVTwQc4QKYzECGAiQ3aObAQAgmMyeWQmaH4nE%2BJGiA4VdlhHllMINJotTBtIbdTCs3ocAbtToS8Z7UTHU4XK43ZAIZYuVYBNQkn4gfyBUW%2BaXQ4KeNCcSn2R6vRV2z5wvlVeG1IolHizS7Ea63OkDL6eUmzCS6ADWKz%2BmEBILBrlNWnmS1Q52h9w4cJpEAZHLK5V56VEEty%2BVGntU%2BM5QsCItibl0HA44wGjebrLG1JCYSmxH4yAInlQh0wxD4ujOIFbw9H49Qk9iXYtNILnIO/HmBnGsyYY5aJFz%2BeRtU3LmAs7HE/QS44K%2Bwu86xAPxCPjk57sv88nEHeTrtFxvLs/6PKy5xZp%2BtTTmMBpkhII6YESeYnnUAwId%2BN6%2BMuGzBLBYYIUSWbQi6XIIm6hYFuy74CqItbNK0vjti27yrr2BD9oOX7XkulrQWun5ntuYyPvuRCvtmH4oeh16/h8AHnCRKKeNBeEgPBo5IYpZHVBRrpUR%2BunFmiRBWuwUyeOGUbLNJC5nD4eK2AArNUjk4hAxmoGBEGuuudTEJg/jEGwhQBV6u5ztxvhYG4cqIeBLI4auVShmpBHxRaFHUXpx7FmkfYDkOSTLNW/JZcWdH1r4sUSlKSr5ZxlyoPxKHWlMsaMMQaqoPccmgZsujEMAWbKZ4eTPhIuzJYavoAO4DZhWgPDmA1DQlMKFiVhkHCqZzoKgFjZAQCDnrmpk2p%2BiKQai6KeAARgN2J4pZKw2T%2BV1KU5LluZqD3ICYm04qQ70op9TluUVnhJP9l1bUpngScDvr%2Bpq2psLqJoQGNWQyt4zn3cQYPuTdeBwlU%2BMQCT%2BaFgjKH8i1ZlDilxpBGa96jQN2M5aVknKicZznPwbBHj58KhAAnI1sRFW2egEzmRW5gMYDSLoyssntB0BcdBhvgZOLSGY3rSI5ChsDItAKKgMhaA41T2JUIaWAUEQW6QBDGAbZgRiAjiOGsvsB4HQePKQRsACymx7DAyAoXCyG7HtmHAsBIEoKhqGQFAQGnqhjuSjz2KQ5yqA0xBcBAt1R0o2RsAQmg8EkUf4P5g54KOXByAoLgNEbpgMMwrCcDIAC0pL2cgtu27QEeGzIJukGbnfR3YTi0J4M2EAgnj/AAHI8w%2BPGHnjAMgyCeKB9gKO7ncMqQ3u%2B/7QdP77Ifh5HS9W9IscgPH1998niAUCoGULnTO3BgHpzzo2Awjgxbx2LjwUu5dK5L2rl0OubAG5NzwC3Fg7co7d0wL3A2/cWBmRHmPXEE97ZTxnqHOe795DLxduvTe2894HyPtAi8fsxZrCYX/T2d8fZ%2B2fs/eh0gI4Lyjp/b%2Bv9E6z2kJfaRH8Y4JxvmYOc8x2AgDDkAA
        const /*static*/ auto format_part = make_overload(
            [](char c_, auto /*ymd_*/, auto /*tod_*/, auto /*tp_*/, auto output_){ *output_ = c_; return 1;},
            [](fmt::y_, auto ymd_, auto /*tod_*/, auto /*tp_*/, auto output_){ utoa_lookup_faster4(output_, int(ymd_.year())); return 4;},
            [](fmt::m_, auto ymd_, auto /*tod_*/, auto /*tp_*/, auto output_){ utoa_lookup_faster2(output_, unsigned(ymd_.month())); return 2;},
            [](fmt::d_, auto ymd_, auto /*tod_*/, auto /*tp_*/, auto output_){ utoa_lookup_faster2(output_, unsigned(ymd_.day())); return 2;},
            [](fmt::H_, auto /*ymd_*/, auto tod_, auto /*tp_*/, auto output_){ utoa_lookup_faster2(output_, tod_.hours().count()); return 2;},
            [](fmt::M_, auto /*ymd_*/, auto tod_, auto /*tp_*/, auto output_){ utoa_lookup_faster2(output_, tod_.minutes().count()); return 2;},
            [](fmt::S_, auto /*ymd_*/, auto tod_, auto /*tp_*/, auto output_){ utoa_lookup_faster2(output_, tod_.seconds().count()); return 2;},
            [](fmt::f_, auto /*ymd_*/, auto /*tod_*/, time_point<system_clock, milliseconds> tp_, auto output_){ utoa_lookup(output_, 3, tp_.time_since_epoch().count() % 1000); return 3;},
            [](fmt::f_, auto /*ymd_*/, auto /*tod_*/, time_point<system_clock, microseconds> tp_, auto output_){ utoa_lookup_faster6(output_, tp_.time_since_epoch().count() % 1000000); return 6;},
            [](fmt::f_, auto /*ymd_*/, auto /*tod_*/, time_point<system_clock, nanoseconds> tp_, auto output_){ utoa_lookup(output_, 9, tp_.time_since_epoch().count() % 1000000000); return 9;}
        );

        char * output = buffer;

        for_each_in_tuple(formatters, [format_part, ymd, tod, tp, &output](auto fmt){
            output += format_part(fmt, ymd, tod, tp, output);
        });

        *(++output) = '\0';
        return buffer;
    }

    const std::tuple<T...> formatters;
    mutable char buffer[64];
};
}

namespace v3
{
namespace fmt
{
    struct y_ {};
    struct m_ {};
    struct d_ {};
    struct H_ {};
    struct M_ {};
    struct S_ {};
    struct f_ {};

namespace detail
{
    using namespace std::chrono;
    using namespace date;

    template<typename TOD, typename Duration>
    int output(char c, year_month_day /*ymd*/, TOD /*tod*/, time_point<system_clock, Duration> /*tp*/, char * output)
    {
        *output = c; return 1;
    }
    template<typename TOD, typename Duration>
    int output(y_ /*y*/, year_month_day ymd, TOD /*tod*/, time_point<system_clock, Duration> /*tp*/, char * output)
    {
        utoa_lookup_faster4(output, int(ymd.year())); return 4;
    }
    template<typename TOD, typename Duration>
    int output(m_ /*m*/, year_month_day ymd, TOD /*tod*/, time_point<system_clock, Duration> /*tp*/, char * output)
    {
        utoa_lookup_faster2(output, unsigned(ymd.month())); return 2;
    }
    template<typename TOD, typename Duration>
    int output(d_ /*d*/, year_month_day ymd, TOD /*tod*/, time_point<system_clock, Duration> /*tp*/, char * output)
    {
        utoa_lookup_faster2(output, unsigned(ymd.day())); return 2;
    }
    template<typename TOD, typename Duration>
    int output(H_ /*H*/, year_month_day /*ymd*/, TOD tod, time_point<system_clock, Duration> /*tp*/, char * output)
    {
        utoa_lookup_faster2(output, tod.hours().count()); return 2;
    }
    template<typename TOD, typename Duration>
    int output(M_ /*M*/, year_month_day /*ymd*/, TOD tod, time_point<system_clock, Duration> /*tp*/, char * output)
    {
        utoa_lookup_faster2(output, tod.minutes().count()); return 2;
    }
    template<typename TOD, typename Duration>
    int output(S_ /*S*/, year_month_day /*ymd*/, TOD tod, time_point<system_clock, Duration> /*tp*/, char * output)
    {
        utoa_lookup_faster2(output, tod.seconds().count()); return 2;
    }
    template<typename TOD>
    int output(f_ /*f*/, year_month_day /*ymd*/, TOD /*tod*/, time_point<system_clock, milliseconds> tp, char * output)
    {
        utoa_lookup(output, 3, tp.time_since_epoch().count() % 1000); return 3;
    }
    template<typename TOD>
    int output(f_ /*f*/, year_month_day /*ymd*/, TOD /*tod*/, time_point<system_clock, microseconds> tp, char * output)
    {
        utoa_lookup_faster6(output, tp.time_since_epoch().count() % 1000000); return 6;
    }
    template<typename TOD>
    int output(f_ /*f*/, year_month_day /*ymd*/, TOD /*tod*/, time_point<system_clock, nanoseconds> tp, char * output)
    {
        utoa_lookup(output, 9, tp.time_since_epoch().count() % 1000000000); return 9;
    }
}
}


template<typename... T>
struct formatter
{
    constexpr formatter(T&&... args)
        : formatters{std::forward<T>(args)...}
        , buffer{}
    {
    }

    template<typename Duration>
    const char * format(Duration duration) const
    {
        using namespace std::chrono;
        using namespace date;

        const time_point<system_clock, Duration> tp{duration};
        const auto dp = floor<days>(tp);
        const auto ymd = year_month_day(dp);
        const auto tod = make_time(tp - dp);


        char * out = buffer;

        for_each_in_tuple(formatters, [ymd, tod, tp, &out](auto fmt){
            out += fmt::detail::output(fmt, ymd, tod, tp, out);
        });

        *(++out) = '\0';
        return buffer;
    }

    const std::tuple<T...> formatters;
    mutable char buffer[64];
};

}


