#pragma once

#include "constants.h"

#include <chrono>
#include <string>

const char * BENCHMARK_NOINLINE format_date(std::chrono::microseconds micros);

std::string BENCHMARK_NOINLINE format_date_pure(std::chrono::microseconds micros);
